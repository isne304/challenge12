#include <iostream>
#include "list.h"
using namespace std;

template <class T>
void swap_values(T& variable1, T& variable2)
{
	T temp;
	temp = variable1;
	variable1 = variable2;
	variable2 = temp;
}
template <class T>
int index_of_smallest(T& a, int start_index, int number_used) {
	int min = a[start_index];
	int index_of_min = start_index;
	for (int index = start_index + 1; index < number_used; index++)
		if (a[index] < min)
		{
			min = a[index];
			index_of_min = index;
		}
	return index_of_min;
}
template <class T>
void sort(T a[], int number_used) {
	int index_of_next_smallest;
	for (int index = 0; index < number_used - 1; index++) {
		index_of_next_smallest = index_of_smallest(a, index, number_used);
		swap_values(a[index], a[index_of_next_smallest]);
	}
}


void main() {
	int num_int[6] = { 1,9,3,4,5,6 };
	double num_double[6] = { 5,9,4,7,10,6 };
	char chars[6] = { 'd','z','c','b','a' };
	sort(num_int, 6);
	cout << " Int short : ";
	for (int i = 0; i < 6; i++) {
		cout << num_int[i] << " ";
	}
	sort(num_double, 6);
	cout << endl << " Double short : ";
	for (int i = 0; i < 6; i++) {
		cout << num_double[i] << " ";
	}
	sort(chars, 6);
	cout << endl << " Char short : ";
	for (int i = 0; i < 6; i++) {
		cout << chars[i] << " ";
	}
	cout << endl;
	cout << endl;



	List list;
	cout << list.headPop() << endl;
	for (int i = 0; i <= 8; i++) {
		list.headPush(i);
		cout << "HeadPush : " << i << endl;
	}
	list.headPush(6);
	cout << "add 6 " << endl;
	list.headPush(4);
	cout << "add 4 " << endl;

	if (list.isInList(2) == 1) { cout << " Found" << endl; }
	list.unique();
	list.sort();
	cout << "sort and unique" << endl;
	if (list.isInList(6) == 1) {
		cout << " Found" << endl;
	}
	for (int i = 6; i > 0; i--) {
		list.headPush(i);
		cout << "HeadPush : " << i << endl;
	}
	if (list.isInList(2) == 1) {
		cout << " Found" << endl;
	}
	for (int i = 7; i < 10; i++) {
		list.tailPush(i);
		cout << "TailPush : " << i << endl;
	}
	if (list.isInList(5) == 0) { cout << " not Found" << endl; }
	else { cout << " Found" << endl; }
	cout << "Delete : 5 " << endl;
	list.sort();
	list.deleteNode(5);
	if (list.isInList(5) == 0) { cout << " not Found" << endl; }
	else { cout << " Found" << endl; }
	list.unique();
	list.sort();
	if (list.isInList(5) == 0) { cout << " not Found" << endl; }
	else { cout << " Found" << endl; }
	system("pause");
}