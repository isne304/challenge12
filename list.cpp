#include <iostream>
#include "list.h"
using namespace std;
List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}
void List::headPush(int list) {
	if (tail == 0 && head == 0) {
		tail = new Node(list, 0, tail);
		head = tail;
	}
	Node *temp;
	temp = new Node(list, 0, head);
	head->next = temp;
	head = temp;
}
void List::tailPush(int list) {
	if (tail == 0 && head == 0) {
		tail = new Node(list, 0, tail);
		head = tail;
	}
	Node *temp;
	temp = new Node(list, 0, tail);
	tail->next = temp;
	tail = temp;
}
int List::headPop() {
	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return false;
	}
	int num;
	Node *tmp = head->next;
	num = head->info;
	head = head->next;
	delete tmp;
	return num;
}
int List::tailPop() {
	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return false;
	}
	int num;
	Node *tmp = head;
	if (head == tail) {
		num = head->info;
		delete tmp;
		head = tail = NULL;
		return num;
	}
	else {
		while (tmp->next->next != NULL) {
			tmp = tmp->next;
		}
		num = tail->info;
		delete tail;
		tmp->next = NULL;
		tail = tmp;
		return num;
	}
}
void List::deleteNode(int n) {
	Node *tmp = head;
	Node *copy;
	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return;
	}
	while (true) {
		if (tmp->info == n) {
			if (tmp == tail) {
				List::tailPop();
				return;
			}
			else if (tmp == head) {
				List::headPop();
				return;
			}
			else {
				copy = head;
				copy = tmp->next;
				delete tmp;
				tmp = copy;
				return;
			}
		}
		if (tmp->next == 0) return;
		tmp = tmp->next;
	}
}
bool List::isInList(int n) {
	if (isEmpty()) {
		cout << "Link List Is Empty! ";
		return false;
	}
	Node *tmp = head;
	while (tmp != NULL) {
		if (tmp->info == n) {
			return true;
		}
		else {
			tmp = tmp->next;
		}
	}
	return false;
}
void List::sort() {
	Node *list = head; // pointer
	int tmp;
	bool swap = false;
	while (true) {
		swap = false;
		while (list != tail) {
			if (list->info > list->next->info) {
				tmp = list->next->info;
				list->next->info = list->info;
				list->info = tmp;
				swap = true;
			}
			list = list->next;
		}
		if (swap == false) return;
		list = head;
	}
}
template <class T>
void List::unique() {
	Node *list1=head,*list2=head;
	T tmp;
	while (true) {
		while (list2->next != 0) {
			if (list1->info == list2->info) {
				tmp = list1->info;
				list1 = list1->next;
				List::deleteNode(tmp);
			}
			list2 = list2->next;
		}
		if (list1 == 0) { return; }
		list2 = head;
	}
}